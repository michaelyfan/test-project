# Use an official Python runtime as a parent image
FROM node:10-alpine

# Set the working directory to /app
RUN mkdir -p /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install dependencies
RUN npm install

# Expose ports on which this program runs
EXPOSE 2020

# Set environment variables for staging
ARG APP_ENV=""
ENV NODE_ENV="${APP_ENV}"

# Run app when the container launches
CMD ["node", "time.js"]