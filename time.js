const express = require('express');
const moment = require('moment');
const app = express();

const PORT = 2020;

app.get('/', (req, res) => {

  const output = `Day of the week is: ${moment().format('dddd')}<br />NODE_ENV is set to:${process.env.NODE_ENV}`;
  res.send(output);
});

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));
